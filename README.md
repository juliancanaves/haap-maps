# very low-level server for haap system maps 

## Usage:
- start server wit 
$ node index.js

### Json to load system
http://localhost:3001/[system-codename]

### System map to show on canvas
http://localhost:3001/[system-codename]/map


# How to Publish a system:
do a Pull Request of a directory with:

### name as system codename (allowed characters: [0-9a-z\-] )

### two files inside
system.json (json with the nodes, connections, ICE)
map.art (ascii art for the Canvas component)


- Sample:
see systems/sample directory
