// this is a very low-level server for haap system maps 
// while developing, start server with $ supervisor index.js 
const http = require('http')
const url = require('url')
const path = require('path')
const fs = require('fs') 

const port = 3001
const mimeTypes = {
    "plain": "text/plain",
    "json": "application/json"
}

const DEBUG = true;

const allowedOrigin = "http://haap.space"

const notFound = (res) => {
    res.writeHead(404, {
        'Access-Control-Allow-Origin': allowedOrigin, 
        'Content-Type': mimeTypes.plain
    })
    res.write('Not Found')
    res.end()
}

const writeMap = (data, res) => {
    res.writeHead(200, {
        'Access-Control-Allow-Origin': allowedOrigin, 
        'Content-Type': mimeTypes.json
    })
    res.write(data)
    res.end()
    if (DEBUG) console.log('.')
}

const fileName = (uri) => {
    if (DEBUG) console.log('requested: '+uri)

    if (uri.substr(0, 5) === '/maps') {
        uri = uri.substr(5)
        if (DEBUG) console.log('requested: '+uri)
    }

    // requested resource (json|art)
    if ('/map' === uri.substr(-4)) {
        // map art
        filename = 'systems'+uri+'.art'
    } else {
        // map json
        filename = 'systems'+uri+'/system.json'
    }

    if (DEBUG) console.log('filename: '+filename)

    // absolute path
    return path.join(__dirname, filename)
}

// this function will be invoked every time a request hits the server
const requestHandler = (req, res) => {

    // resource
    uri = url.parse(req.url).pathname

    fs.readFile(fileName(uri), (err, data) => {
        if(err) {
            console.log('ERROR: reading file: '+fileName(uri)+' for '+uri+' --> '+err.code)
            notFound(res)
            return
        }

        writeMap(data, res)
    })

    return
}

const server = http.createServer(requestHandler)

server.listen(port, (err) => {
    if (err) {
        return console.log('ERROR: ', err)
    }    

    console.log(`server is listening on ${port}`)
})
